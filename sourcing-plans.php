<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies"><title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/global/global.css" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

        <!--========== HEADER ==========-->
        <?php include_once("analytics.php") ?>
        <?php include_once("header.php") ?>
        <!--========== END HEADER ==========-->

        <!--========== PROMO BLOCK ==========-->
        <section class="s-video-v2__bg" data-vidbg-bg="poster: img/1920x1080/sourcing_prices.jpg" data-vidbg-options="loop: true, muted: true, overlay: false">
            <div class="container g-position--overlay g-text-center--xs">
                <div class="g-padding-y-50--xs g-margin-t-100--xs g-margin-b-100--xs g-margin-b-250--md">
                    <h1 class="g-font-size-36--xs g-font-size-50--sm g-font-size-60--md g-color--white">Sourcing</h1>
                    <h2 class="g-font-size-36--xs g-font-size-50--sm g-font-size-60--md g-color--white">plans</h2>
                </div>
            </div>
        </section>
        <!--========== END PROMO BLOCK ==========-->

        <!--========== PAGE CONTENT ==========-->
        <!-- Mockup -->
        <div class="container g-margin-t-o-150--xs">
            <div class="center-block s-mockup-v1">
                <img class="img-responsive" src="img/mockups/items-01.png" alt="services Image">
            </div>
        </div>
        <!-- End Mockup -->

        <!-- End Portfolio -->
        <!-- Plan -->
        <div class="g-color--primary">
            <div class="container g-padding-y-80--xs g-padding-y-125--xsm">
                <div class="g-text-center--xs g-margin-b-80--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Plan</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--md">Finding your Plan</h2>
                </div>

                <div class="row g-row-col--5">
                    <!-- Plan -->
      <!--               <div class="col-md-6 g-margin-b-10--xs g-margin-b-0--lg">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                            <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                                <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-archive"></i>
                                <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">STARTUP</h3>
                                <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-40--xs">
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> 1 PRODUCT CATEGORY</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> FACTORY ANALYSIS</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> COMPANY INFORMATION</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> PRODUCT CERTIFICATES</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> PROTOTYPE / SAMPLE</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> FIRST ORDER SUPPORT</li>

                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> LOGISITCS SUPPORT (Freightrates-integrated service)</li>
                                </ul>
                                <div class="g-margin-b-40--xs">
                                    <span class="s-plan-v1__price-mark">$</span>
                                    <span class="s-plan-v1__price-tag"> 299 </span>
                                    <p>(Flat Fee towards  Vendor development )  + % service Charge if order value > $ 10000</p>
                                </div>
                               <div class="g-text-center--xs">
                                <a href="contacts.php" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Contact Us</a>
                              </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- End Plan -->

                    <!-- Plan -->
<!--                     <div class="col-md-6 g-margin-b-10--xs g-margin-b-0--lg">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                            <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                                <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-package"></i>
                                <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">ENTERPRISE</h3>
                                <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-40--xs">
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> 1 PRODUCT CATEGORY</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> FACTORY ANALYSIS</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> COMPANY INFORMATION</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> PRODUCT CERTIFICATES</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> PROTOTYPE / SAMPLE</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> FIRST ORDER SUPPORT</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> REORDER SUPPORT</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> RATE CONTRACT SUPPORT *</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> LOGISITCS SUPPORT (Freightrates-integrated service)</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> THIRD PARTY INSPECTION SERVICES**
                                    </li>
                                </ul>
                                <div class="g-margin-b-40--xs">
                                    <span class="s-plan-v1__price-mark">$</span>
                                    <span class="s-plan-v1__price-tag"> 199 </span>
                                    <p>(Flat Fee towards vendor development- first order only ) + % service Charge if order value > $ 10000</p>
                                </div>
                               <div class="g-text-center--xs">
                                <a href="contacts.php" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Contact Us</a>
                              </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- End Plan -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th>STARTUP</th>
                                            <th>ENTERPRISE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>PRODUCT CATEGORY</td>
                                            <td>1</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>FACTORY ANALYSIS</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>COMPANY INFORMATION</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>PRODUCT CERTIFICATES</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>PROTOTYPE / SAMPLE</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>FIRST ORDER SUPPORT</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>REORDER SUPPORT</td>
                                            <td>-</td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>RATE CONTRACT SUPPORT *</td>
                                            <td>-</td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>LOGISITCS SUPPORT (Freightrates-integrated service)</td>
                                            <td><i class="ti-check"></i> </td>
                                            <td><i class="ti-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>THIRD PARTY INSPECTION SERVICES**</td>
                                            <td>-</td>
                                            <td><i class="ti-check"></i>ON REQUEST</td>
                                        </tr>
                                        <tr>
                                            <td>PRICE</td>
                                            <td>$ 299 
                                            <br> (Flat Fee towards  Vendor development )  <br> + % service Charge if order value > $ 10000</td>
                                            <td>$ 199
                                            <br> (Flat Fee towards vendor development- first order only ) <br> + % service Charge if order value > $ 10000</td>
                                        </tr>
                                        <tr>
                                            <td>REORDER PRICE</td>
                                            <td></td>
                                            <td>Refer Prices in Reorder Table</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <p>
                    *Rate contract support are subject to product category and the contract period
                    <br>
                    ** Third party inspection is provided by reputed inspection agencies (SGS / BV ) at additional cost
                    </p>
                </div>
            </div>
        </div>

        <div class="g-color--primary">
            <div class="container g-padding-y-80--xs g-padding-y-125--xsm">
               <div class="g-text-center--xs g-margin-b-80--xs">
                    <h2 class="g-font-size-32--xs g-font-size-36--md"> Prices in Reorder </h2>
                </div>

                <div class="row g-row-col--5">
                <!-- End Plan -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>Order Amount</th>
                                            <th>Service Charge First order</th>
                                            <th>Service Charge Reorder</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>less than $1,000</td>
                                            <td></td>
                                            <td>$100</td>
                                        </tr>
                                        <tr>
                                            <td> $1,000-$2,000</td>
                                            <td> </td>
                                            <td>8%</td>
                                        </tr>
                                        <tr>
                                            <td> $2,000-$3,000</td>
                                            <td> </td>
                                            <td> 7%  </td>
                                        </tr>
                                        <tr>
                                            <td> $3,000-$5,000</td>
                                            <td> </td>
                                            <td>6.50%</td>
                                        </tr>
                                        <tr>
                                            <td> $5,000-$7,000</td>
                                            <td> </td>
                                            <td>6%</td>
                                        </tr>
                                        <tr>
                                            <td> $7,000-$10,000</td>
                                            <td></td>
                                            <td>5.50%</td>
                                        </tr>
                                        <tr>
                                            <td> $10,000-$15,000</td>
                                            <td>7%</td>
                                            <td>5%</td>
                                        </tr>
                                        <tr>
                                            <td>$15,000-$20,000</td>
                                            <td>6.50%</td>
                                            <td>4.50%</td>
                                        </tr>
                                        <tr>
                                            <td>$20,000-$25,000</td>
                                            <td>6%</td>
                                            <td>4%</td>
                                        </tr>
                                        <tr>
                                            <td> $25,000-$30,000</td>
                                            <td>5.50%</td>
                                            <td>3.50%</td>
                                        </tr>
                                        <tr>
                                            <td>> $30,000</td>
                                            <td>5%</td>
                                            <td>2%</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Plan -->
        <!--========== END PAGE CONTENT ==========-->

        <!--========== FOOTER ==========-->
        <?php include_once("footer.php") ?>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
        <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="vendor/vidbg.min.js"></script>
        <script type="text/javascript" src="vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
        <script type="text/javascript" src="vendor/waypoint.min.js"></script>
        <script type="text/javascript" src="vendor/counterup.min.js"></script>
        <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="js/global.min.js"></script>
        <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
        <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
        <script type="text/javascript" src="js/components/portfolio-4-col-slider.min.js"></script>
        <script type="text/javascript" src="js/components/counter.min.js"></script>
        <script type="text/javascript" src="js/components/swiper.min.js"></script>
        <script type="text/javascript" src="js/components/wow.min.js"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
