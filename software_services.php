<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Casfer Technologies</title>
        <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
        <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
        <meta name="author" content="CasFer Technologies">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />

        <!-- Theme Styles -->
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/global/global.css" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->

        <!--========== SWIPER SLIDER ==========-->
        <div class="s-swiper js__swiper-slider">
            <!-- Swiper Wrapper -->
            <div class="swiper-wrapper">
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/software_Services_background_1.jpg');">
                    <div class="container g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="g-margin-b-50--xs">
                                    <h1 class="g-font-size-32--xs g-font-size-45--sm g-font-size-60--md g-color--white">Software services<br>that You Can Trust</h1>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">
                                    Our in house team of software developers can tackle any issue.</p>
                                </div>
                                <a href="#js__scroll-to-section" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/software_Services_background_3.jpg');">
                    <div class="container g-text-right--xs g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="g-margin-b-50--xs">
                                    <h1 class="g-font-size-32--xs g-font-size-45--sm g-font-size-60--md g-color--white">Software services<br>that You Can Trust</h1>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white">
                                    Our in house team of software developers can tackle any issue.</p>
                                </div>
                                <a href="#js__scroll-to-section" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Swiper Wrapper -->

            <!-- Pagination -->
            <div class="s-swiper__pagination-v1 s-swiper__pagination-v1--bc s-swiper__pagination-v1--white js__swiper-pagination"></div>
        </div>
        <!--========== END SWIPER SLIDER ==========-->

        <!--========== PAGE CONTENT ==========-->
        <!-- Services -->
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="row g-margin-b-10--xs">
                <div class="col-md-6 g-margin-b-60--xs g-margin-b-0--lg">
                    <!-- Masonry Grid -->
                    <div class="row g-row-col--5 g-overflow--hidden js__masonry">
                        <div class="col-xs-6 js__masonry-sizer"></div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs g-margin-b-0--sm js__masonry-item">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="img-responsive" src="img/services/Delphi-logo.png" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".3s">
                                <img class="img-responsive" src="img/services/AngularJS-logo.png" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".5s">
                                <img class="img-responsive" src="img/services/Android-logo.png" alt="Image">
                            </div>
                        </div>
                    </div>
                    <!-- End Masonry Grid -->
                </div>
                <div class="col-md-5 g-margin-b-10--xs g-margin-b-0--lg g-margin-t-10--lg g-margin-l-20--lg" id="js__scroll-to-section">
                    <div class="g-margin-b-30--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-15--xs">Services</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--sm">We Specialize In</h2>
                        <p>in Desktop, web and mobile application development</p>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled col-xs-4 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>ERP Solutions</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>POS Solutions</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>CRM Solutions</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Quality Assurance</li>
                        </ul>
                        <ul class="list-unstyled col-xs-4 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Custom Software Development</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Software Version Migration</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Medical Solutions</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Website Design</li>
                        </ul>
                        <ul class="list-unstyled col-xs-4 g-full-width--xs g-ul-li-tb-5--xs">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Offshore Development</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Support and Maintenance</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Software Application Design</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Software Outsourcing</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Services -->

        <!-- Process -->
        <div class="g-bg-color--primary-ltr">
            <div class="container g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-text-center--xs g-margin-b-100--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Process</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--sm g-color--white">How it Works</h2>
                </div>
                <ul class="list-inline row g-margin-b-100--xs">
                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Requirement Gathering</h3>
                                <p class="g-color--white-opacity"></p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Offer Proposal</h3>
                                <p class="g-color--white-opacity"></p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Develop</h3>
                                <p class="g-color--white-opacity"></p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->

                    <!-- Process -->
                    <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                        <div class="center-block g-text-center--xs">
                            <div class="g-margin-b-30--xs">
                                <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                            </div>
                            <div class="g-padding-x-20--xs">
                                <h3 class="g-font-size-18--xs g-color--white">Maintain & Enhance</h3>
                                <p class="g-color--white-opacity"></p>
                            </div>
                        </div>
                    </li>
                    <!-- End Process -->
                </ul>

                <div class="g-text-center--xs">
                    <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                        <a href="#js__scroll-to-appointment" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50">Hire Us</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Process -->

         <div class="g-container--md g-padding-y-80--xs g-padding-y-100--sm">
            <div class="s-swiper js__swiper-clients">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".1s">
                            <img class="s-clients-v1" src="img/services-logos/nodejs.png" alt="NodeJS">
                            Node.JS
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".2s">
                            <img class="s-clients-v1" src="img/services-logos/angular.png" alt="angularJS">
                            AngularJS
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".3s">
                            <img class="s-clients-v1" src="img/services-logos/mean-stack.png" alt="mean-stack">
                            MEAN stack
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".4s">
                            <img class="s-clients-v1" src="img/services-logos/php.png" alt="php">
                            PHP
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".5s">
                            <img class="s-clients-v1" src="img/services-logos/mongodb.png" alt="mongodb">
                            MongoDB
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="g-container--md g-padding-y-80--xs g-padding-y-100--sm">
            <div class="s-swiper js__swiper-clients">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".1s">
                            <img class="s-clients-v1" src="img/services-logos/delphi.png" alt="delphi">
                            Delphi
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".2s">
                            <img class="s-clients-v1" src="img/services-logos/ms-sql.png" alt="MS SQL server">
                            MS SQL server
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".3s">
                            <img class="s-clients-v1" src="img/services-logos/mysql.png" alt="mysql">
                            MySQL
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".4s">
                            <img class="s-clients-v1" src="img/services-logos/postgresql.png" alt="postgresql">
                            PostgreSQL
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".5s">
                            <img class="s-clients-v1" src="img/services-logos/electron-logo.png" alt="electron">
                            Electron
                        </div>
                    </div>
                </div>
            </div>
        </div>


<!--         <div class="container g-text-center--xs g-padding-y-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">30 years caring about you</p>
            <h2 class="g-font-size-32--xs g-font-size-36--sm">Professional Specialists</h2>
        </div> -->
        <!-- Team -->
<!--          <div class="row g-row-col--0">
            <div class="col-md-3 col-xs-6 g-full-width--xs">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
  
                    <div class="s-team-v1">
                        <img class="img-responsive g-width-100-percent--xs" src="img/400x400/03.jpg" alt="Image">
                        <div class="g-text-center--xs g-bg-color--white g-padding-x-30--xs g-padding-y-40--xs">
                            <h2 class="g-font-size-18--xs g-margin-b-5--xs">Jack Daniels</h2>
                            <span class="g-font-size-15--xs g-color--text"><i>Cardiology</i></span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                 
                    <div class="s-team-v1">
                        <img class="img-responsive g-width-100-percent--xs" src="img/400x400/04.jpg" alt="Image">
                        <div class="g-text-center--xs g-bg-color--white g-padding-x-30--xs g-padding-y-40--xs">
                            <h3 class="g-font-size-18--xs g-margin-b-5--xs">Anna Kusaikina</h3>
                            <span class="g-font-size-15--xs g-color--text"><i>Pulmonology</i></span>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".3s">
                    
                    <div class="s-team-v1">
                        <img class="img-responsive g-width-100-percent--xs" src="img/400x400/05.jpg" alt="Image">
                        <div class="g-text-center--xs g-bg-color--white g-padding-x-30--xs g-padding-y-40--xs">
                            <h4 class="g-font-size-18--xs g-margin-b-5--xs">Lucas Richardson</h4>
                            <span class="g-font-size-15--xs g-color--text"><i>Dental</i></span>
                        </div>
                    </div>
                   
                </div>
            </div>
            <div class="col-md-3 col-xs-6 g-full-width--xs">
                <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".4s">
                   
                    <div class="s-team-v1">
                        <img class="img-responsive g-width-100-percent--xs" src="img/400x400/06.jpg" alt="Image">
                        <div class="g-text-center--xs g-bg-color--white g-padding-x-30--xs g-padding-y-40--xs">
                            <h4 class="g-font-size-18--xs g-margin-b-5--xs">Kira Doe</h4>
                            <span class="g-font-size-15--xs g-color--text"><i>Gynecology</i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>  -->
        <!-- End Team -->

        <!-- Parallax -->
        <div class="s-promo-block-v5 g-bg-position--center js__parallax-window" style="background: url(img/1920x1080/software_Services_background_3.jpg) 50% 0 no-repeat fixed;">
            <div class="container g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-margin-b-80--xs">
                    <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white">Looking for un disrupting supply chain through technology?</h2>
                </div>
                <a href="sourcing_services.php" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50">Find out</a>
            </div>
        </div>
        <!-- End Parallax -->

        <!-- News -->
        <div class="container-fluid g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-80--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Projects</p>
                <p>A glance into some of the <br> of the completed projects.</p>
            </div>

            <!-- Swiper -->
            <div class="s-swiper js__swiper-news">
                <!-- Wrapper -->
                <div class="swiper-wrapper g-margin-b-60--xs">
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/projects/empowering-goa.png');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Empowering Goans</h3>
                                <p class="g-color--white">Giving power of media to Goans.</p>
                            </div>
                            <a href="http://empoweringgoans.com/" target="_blank" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Visit</a>
                        </div>
                    </article>
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/projects/freightates.png');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">FreightRates.in</h3>
                                <p class="g-color--white">Bringing customers and suppliers together</p>
                            </div>
                            <a href="http://freightrates.in/" target="_blank" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Visit</a>
                        </div>
                    </article>
<!--                     <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/400x500/09.jpg');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Medical Services</h3>
                                <p class="g-color--white">Clinics can be privately operated.</p>
                            </div>
                            <a href="#" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Read More</a>
                        </div>
                    </article>
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/400x500/10.jpg');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Medical Services</h3>
                                <p class="g-color--white">Clinics can be privately operated.</p>
                            </div>
                            <a href="#" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Read More</a>
                        </div>
                    </article>
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/400x500/11.jpg');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Medical Services</h3>
                                <p class="g-color--white">Clinics can be privately operated.</p>
                            </div>
                            <a href="#" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Read More</a>
                        </div>
                    </article>
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/400x500/12.jpg');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Medical Services</h3>
                                <p class="g-color--white">Clinics can be privately operated.</p>
                            </div>
                            <a href="#" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Read More</a>
                        </div>
                    </article>
                    <article class="s-promo-block-v6 g-bg-position--center swiper-slide" style="background: url('img/400x500/13.jpg');">
                        <div class="g-text-center--xs g-padding-x-15--xs g-padding-x-30--lg g-padding-y-50--xs g-margin-t-120--xs">
                            <div class="g-margin-b-25--xs">
                                <h3 class="g-font-size-16--xs g-color--white g-margin-b-5--xs">Medical Services</h3>
                                <p class="g-color--white">Clinics can be privately operated.</p>
                            </div>
                            <a href="#" class="text-uppercase s-btn s-btn--xs s-btn--white-brd g-radius--50">Read More</a>
                        </div>
                    </article> -->
                </div>
                <!-- End Wrapper -->

                <!-- Pagination -->
                <div class="s-swiper__pagination-v1 s-swiper__pagination-v1--dark g-text-center--xs js__swiper-pagination"></div>
            </div>
            <!-- End Swiper -->
        </div>
        <!-- End News -->

        <!-- Form -->
        <div id="js__scroll-to-appointment" class="g-bg-color--sky-light g-padding-y-80--xs g-padding-y-125--sm">
            <div class="container g-bg-color--white g-box-shadow__dark-lightest-v3">
                <div class="row">
                    <!-- Form -->
                    <div class="col-md-8 js__form-eqaul-height-v1">
                        <div class="g-padding-x-40--xs g-padding-y-50--xs">
                            <h2 class="g-font-size-24--xs g-color--primary g-margin-b-50--xs">Get a free quote</h2>
                            <form>
                                <div class="row g-margin-b-30--xs g-margin-b-50--md">
                                    <div class="col-sm-6 g-margin-b-30--xs g-margin-b-0--md">
                                        <input type="text" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Full Name" id="name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Phone Number" id="phone">
                                    </div>
                                </div>
                                <div class="row g-margin-b-50--xs g-margin-b-50--md">
                                    <div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">
                                        <input type="email" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Email" id="email">
                                    </div>
                                </div>
                                <div class="g-margin-b-50--xs">
                                    <textarea class="form-control s-form-v4__input g-padding-l-0--xs" rows="5" placeholder="* What are your requirement?" id="requirement"></textarea>
                                </div>
                                <div class="g-text-center--xs">
                                    <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-70--xs g-margin-b-20--xs" id="btnSoftwareRequirement">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Form -->

                    <!-- Contacts -->
                    <div class="col-md-4 g-bg-color--primary-ltr js__form-eqaul-height-v1">
                        <div class="g-overflow--hidden g-padding-x-40--xs g-padding-y-50--xs">
                            <h2 class="g-font-size-24--xs g-color--white g-margin-b-50--xs">Contact Us</h2>
                            <ul class="list-unstyled g-margin-b-70--xs">
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-location-pin"></i>
                                    </div>
                                    <div class="g-media__body">
                                        Veroda, Cuncolim Salcete, <br> Goa INDIA
                                    </div>
                                </li>
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-headphone-alt"></i>
                                    </div>
                                    <div class="g-media__body">
                                        <a href="tel:+918408881080" style="color:white">+91 8408881080</a><br>
                                        <a href="tel:+917719855705" style="color:white">+91 7719855705</a><br>
                                        <a href="tel:+917387352248" style="color:white">+91 7387352248</a><br>
                                    </div>
                                </li>
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-email"></i>
                                    </div>
                                    <div class="g-media__body">
                                        business@casfertechnologies.com
                                    </div>
                                </li>
                            </ul>
                            <ul class="list-inline g-ul-li-lr-15--xs">
                                <li><a href="https://www.facebook.com/casfertech"><i class="g-font-size-20--xs g-color--white-opacity ti-facebook"></i></a></li>
                                <li><a href="https://twitter.com/casfertech"><i class="g-font-size-20--xs g-color--white-opacity ti-twitter"></i></a></li>
                                <li><a href="skype:casfertechnologies"><i class="g-font-size-20--xs g-color--white-opacity ti-skype"></i></a></li>
                            </ul>
                            <i class="g-font-size-150--xs g-color--white-opacity-lightest ti-comments" style="position: absolute; bottom: -1.25rem; right: -1.25rem;"></i>
                        </div>
                    </div>
                    <!-- End Contacts -->
                </div>
            </div>
        </div>
        <!-- End Form -->
        <!--========== END PAGE CONTENT ==========-->

        <!--========== FOOTER ==========-->
        <?php include_once("footer.php") ?>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
        <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/jquery.masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.equal-height.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="js/global.min.js"></script>
        <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
        <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
        <script type="text/javascript" src="js/components/swiper.min.js"></script>
        <script type="text/javascript" src="js/components/masonry.min.js"></script>
        <script type="text/javascript" src="js/components/equal-height.min.js"></script>
        <script type="text/javascript" src="js/components/parallax.min.js"></script>
        <script type="text/javascript" src="js/components/wow.min.js"></script>
        <script type="text/javascript" src="vendor/sweetalert/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="js/mails-sendgrid.js"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
