/*
Description:

*/

(function() {
    'use strict';
    angular
        .module('webApp')
        .config(config)

    config.$inject = ['$stateProvider', '$httpProvider']

    function config($stateProvider, $httpProvider) {

        $stateProvider
            .state('/', {
                url: '/',
                templateUrl: 'shared/index/index.view.html',
                controller: 'Index.Controller',
                controllerAs: 'vm'
            })
            .state('index', {
                url: '/index',
                templateUrl: 'shared/index/index.view.html',
                controller: 'Index.Controller',
                controllerAs: 'vm'
            })
            // .state('home', {
            //     url: '/',
            //     templateUrl: 'shared/home/home.view.html',
            //     controller: 'Home.Controller',
            //     controllerAs: 'vm'
            // })
    }
})();
