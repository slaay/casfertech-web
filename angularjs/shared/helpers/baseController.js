/*
Description: This is the base controller from which all the other controllers will be inherited. 
 Things to check in the base controller is 
 1] Internet is available 
 2] Redirect the user to correct screen
 
Link : https://gist.github.com/shahzadns/522454c184d6e0353718

NOTE:
Base controller must be added in all controllers.
*/


(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('baseController', baseController);

    function baseController($scope, $location) {
        var vm = this;

        activate();

        function activate() {
        };

        /*
        Angular destroys all listeners on scope, by itself, before destroying any scope of a controller, and 
        after triggering $destroy event on that scope.
        */
        $scope.$on('$destroy', scopeDestroyListener);

        //listener for '$destroy' event on scope.
        function scopeDestroyListener() {
            //invokes $scope.onDestroy method ( if defined in child controller ).
            /*
                This is specifically used to destroy any third-party component, that was integrated in any dedicated view of application
                for example: d3 graphs, Google Maps, Jquery's Carousel etc.
                $rootscope listeners could be destroyed here, as well.
            */
            $scope.onDestroy && $scope.onDestroy();
            //logging for development purpose
        }
    }
})();
