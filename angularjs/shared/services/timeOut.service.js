/*
Description: This service is to set default timeout for all APIs.
This service will set timeout in front end for all http requests.
If API doesn't respond in specified time, then it throws an error with status code of -1.
(Default status code for timeout is 408)

reference:http://stackoverflow.com/a/15702500
*/


(function () {
    'use strict';

    angular.module('webApp')
        .factory('timeoutHttpIntercept', Service);

    Service.$inject = ['$rootScope', '$q'];

    function Service($rootScope, $q) {
        return {
            'request': function (config) {
                config.timeout = 60000;  //Default timeout is set to 60000 ms  (1 min).	
                return config;
            }
        };
    }

})();
