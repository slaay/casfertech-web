// login.controller.js

(function () {
    'use strict';
    angular
        .module('webApp')
        .controller("Index.Controller", Controller)

    Controller.$inject = ['$controller', '$scope'];

    function Controller($controller, $scope) {
        //Inherit from the base controller
        $controller('baseController', { $scope: $scope });

        var vm = this;
        activate();

        function activate() {
        }

    }
})();