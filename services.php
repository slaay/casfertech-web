<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies"><title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/global/global.css" rel="stylesheet" type="text/css"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

        <!--========== HEADER ==========-->
        <?php include_once("analytics.php") ?>
        <?php include_once("header.php") ?>
        <!--========== END HEADER ==========-->

        <!--========== PROMO BLOCK ==========-->
        <section class="s-video-v2__bg" data-vidbg-bg="mp4: include/media/mp4_video.mp4, webm: include/media/webm_video.webm, poster: include/media/fallback.jpg" data-vidbg-options="loop: true, muted: true, overlay: false">
            <div class="container g-position--overlay g-text-center--xs">
                <div class="g-padding-y-50--xs g-margin-t-100--xs g-margin-b-100--xs g-margin-b-250--md">
                    <h1 class="g-font-size-36--xs g-font-size-50--sm g-font-size-60--md g-color--white">Services</h1>
                    <h2 class="g-font-size-36--xs g-font-size-50--sm g-font-size-60--md g-color--white">Software & Sourcing</h2>
                </div>
            </div>
        </section>
        <!--========== END PROMO BLOCK ==========-->

        <!--========== PAGE CONTENT ==========-->
        <!-- Mockup -->
        <div class="container g-margin-t-o-150--xs">
            <div class="center-block s-mockup-v1">
                <img class="img-responsive" src="img/mockups/items-01.png" alt="services Image">
            </div>
        </div>
        <!-- End Mockup -->

        <!-- Portfolio -->
        <div class="container g-padding-y-80--xs g-padding-y-125--xsm">
            <div class="row g-margin-b-30--xs">
                <div class="col-sm-4">
                    <div class="g-margin-t-20--md g-margin-b-40--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Services provided by us</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--md">Services types</h2>
                        <p>We are masters of most current technologies.<br>Check us out and enjoy things that we know we're good at.</p>
                    </div>
                </div>

                <div class="col-sm-8">
                    <!-- Portfolio Gallery -->
                    <div id="js__grid-portfolio-gallery" class="s-portfolio__paginations-v1 cbp">
                        <!-- Item -->
                        <div class="s-portfolio__item cbp-item motion graphic">
                            <div class="s-portfolio__img-effect">
                                <img src="img/services/sourcing.jpg" alt="Portfolio Image">
                            </div>
                            <div class="s-portfolio__caption-hover--cc">
                                <div class="g-margin-b-25--xs">
                                    <h3 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Sourcing services</h3>
                                </div>
                                <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                                    <li>
                                        <a href="img/services/sourcing.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Sourcing services">
                                            <i class="ti-fullscreen"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="sourcing_services.php" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                            <i class="ti-link"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="s-portfolio__item cbp-item logos graphic">
                            <div class="s-portfolio__img-effect">
                                <img src="img/services/software_2.jpg" alt="Software services">
                            </div>
                            <div class="s-portfolio__caption-hover--cc">
                                <div class="g-margin-b-25--xs">
                                    <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Software services</h4>
                                </div>
                                <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                                    <li>
                                        <a href="img/services/software_2.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Software services">
                                            <i class="ti-fullscreen"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="software_services.php" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                            <i class="ti-link"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Portfolio Gallery -->
                </div>
            </div>
        </div>
        <!-- End Portfolio -->
        <!-- Plan -->
        <div class="g-bg-color--sky-light">
            <div class="container g-padding-y-80--xs g-padding-y-125--xsm">
                <div class="g-text-center--xs g-margin-b-80--xs">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Plan</p>
                    <h2 class="g-font-size-32--xs g-font-size-36--md">Finding your Plan</h2>
                </div>

                <div class="row g-row-col--5">
                    <!-- Plan -->
                    <div class="col-md-4 g-margin-b-10--xs g-margin-b-0--lg">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".1s">
                            <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                                <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-archive"></i>
                                <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">Basic Plan</h3>
                                <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-40--xs">
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Basic theme</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Social Media integration</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> SEO</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Training</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Blog</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> 1 month support</li>
                                </ul>
                                <div class="g-margin-b-40--xs">
                                    <span class="s-plan-v1__price-mark">Rs</span>
                                    <span class="s-plan-v1__price-tag">15,000</span>
                                </div>
                               <div class="g-text-center--xs">
                                <a href="contacts.php" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Contact Us</a>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Plan -->

                    <!-- Plan -->
                    <div class="col-md-4 g-margin-b-10--xs g-margin-b-0--lg">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".2s">
                            <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                                <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-package"></i>
                                <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">Medium Plan</h3>
                                <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-40--xs">
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Basic theme</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Social Media integration</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> SEO</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Training</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Blog</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> 3 month support</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Payment gateway</li>
                                </ul>
                                <div class="g-margin-b-40--xs">
                                    <span class="s-plan-v1__price-mark">Rs </span>
                                    <span class="s-plan-v1__price-tag">20, 000</span>
                                </div>
                               <div class="g-text-center--xs">
                                <a href="contacts.php" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Contact Us</a>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Plan -->
                    
                    <!-- Plan -->
                    <div class="col-md-4">
                        <div class="wow fadeInUp" data-wow-duration=".3" data-wow-delay=".3s">
                            <div class="s-plan-v1 g-text-center--xs g-bg-color--white g-padding-y-100--xs">
                                <i class="g-display-block--xs g-font-size-40--xs g-color--primary g-margin-b-30--xs ti-gift"></i>
                                <h3 class="g-font-size-18--xs g-color--primary g-margin-b-30--xs">Premium Plan</h3>
                                <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-40--xs">
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Basic theme</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Social Media integration</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> SEO</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Training</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Blog</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> 6 month support</li>
                                    <li><i class="g-font-size-13--xs g-color--primary g-margin-r-10--xs ti-check"></i> Payment gateway</li>
                                </ul>
                                <div class="g-margin-b-40--xs">
                                    <span class="s-plan-v1__price-mark">Rs</span>
                                    <span class="s-plan-v1__price-tag">25, 000</span>
                                </div>
                               <div class="g-text-center--xs">
                                <a href="contacts.php" class="text-uppercase s-btn s-btn--sm s-btn--primary-bg g-radius--50 g-padding-x-50--xs">Contact Us</a>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Plan -->
                </div>
            </div>
        </div>
        <!-- End Plan -->
        <!--========== END PAGE CONTENT ==========-->

        <!--========== FOOTER ==========-->
        <?php include_once("footer.php") ?>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
        <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="vendor/vidbg.min.js"></script>
        <script type="text/javascript" src="vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
        <script type="text/javascript" src="vendor/waypoint.min.js"></script>
        <script type="text/javascript" src="vendor/counterup.min.js"></script>
        <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="js/global.min.js"></script>
        <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
        <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
        <script type="text/javascript" src="js/components/portfolio-4-col-slider.min.js"></script>
        <script type="text/javascript" src="js/components/counter.min.js"></script>
        <script type="text/javascript" src="js/components/swiper.min.js"></script>
        <script type="text/javascript" src="js/components/wow.min.js"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
