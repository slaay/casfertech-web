<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Begin Head -->

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">
    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
    <!-- Vendor Styles -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css" />
    <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme Styles -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/global/global.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
</head>
<!-- End Head -->
<!-- Body -->

<body>
    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->
    <!--========== PROMO BLOCK ==========-->
    <div class="g-fullheight--md js__parallax-window" style="background: url(img/1920x1080/business-process.jpg) 50% 0 no-repeat fixed;">
        <div class="g-container--md g-text-center--xs g-ver-center--md g-padding-y-150--xs g-padding-y-0--md">
            <div class="g-margin-b-60--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Welcome to Casfer Technologies</p>
                <h1 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white g-letter-spacing--1">Business process</h1>
                <p class="g-font-size-18--xs g-font-size-26--md g-color--white-opacity g-margin-b-0--xs">We Create Beautiful Experiences
                    <br> That Drive Successful Businesses.</p>
            </div>
            <span class="g-display-block--xs g-display-inline-block--sm g-padding-x-5--xs g-margin-b-10--xs g-margin-b-0--sm">
                    <a href="#js__scroll-to-section" class="text-uppercase s-btn s-btn-icon--md s-btn--white-brd g-radius--50 g-padding-x-65--xs">Learn more</a>
                </span>
        </div>
    </div>
    <!--========== END PROMO BLOCK ==========-->
    <!-- Process -->
    <div class="g-bg-color--primary-ltr" id="js__scroll-to-section">
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-100--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Sourcing</p>
                <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">How it Works</h2>
            </div>
            <ul class="list-inline row g-margin-b-100--xs">
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Preliminary Evaluation</h3>
                            <h4 class="g-color--white-opacity">Plan sourcing process and create commodity profile</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Supply market analysis</h3>
                            <h4 class="g-color--white-opacity">Analyze sourcing options and Create proposal /Inquiry</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Sourcing</h3>
                            <h4 class="g-color--white-opacity">Shortlisting sources based on competencies and capabilities</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Implementation</h3>
                            <h4 class="g-color--white-opacity">Finalizing contracts/agreements, monitor progress and evaluate performance</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
            </ul>
            <div class="g-text-center--xs">
                <a href="contacts.php" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50 g-padding-x-70--xs">Hire Us</a>
            </div>
        </div>
    </div>
    <div class="g-bg-color--primary-to-blueviolet-ltr" id="js__scroll-to-section">
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-100--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Software development</p>
                <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">How it Works</h2>
            </div>
            <ul class="list-inline row g-margin-b-100--xs">
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">01</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Requirement Gathering</h3>
                            <h4 class="g-color--white-opacity">Sign NDA, get the proper picture of your idea</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--md">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">02</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Offer Proposal</h3>
                            <h4 class="g-color--white-opacity">Sign the offer, decide on the meeting time.</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1 g-margin-b-60--xs g-margin-b-0--sm">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">03</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Develop</h3>
                            <h4 class="g-color--white-opacity">Develop your idea with the best of development practices, Test, test, test and deploy!</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
                <!-- Process -->
                <li class="col-sm-3 col-xs-6 g-full-width--xs s-process-v1">
                    <div class="center-block g-text-center--xs">
                        <div class="g-margin-b-30--xs">
                            <span class="g-display-inline-block--xs g-width-100--xs g-height-100--xs g-font-size-38--xs g-color--primary g-bg-color--white g-box-shadow__dark-lightest-v4 g-padding-x-20--xs g-padding-y-20--xs g-radius--circle">04</span>
                        </div>
                        <div class="g-padding-x-20--xs">
                            <h3 class="g-font-size-18--xs g-color--white">Maintenance and Enhancements</h3>
                            <h4 class="g-color--white-opacity">Help you improve your idea even more!</h4>
                        </div>
                    </div>
                </li>
                <!-- End Process -->
            </ul>
            <div class="g-text-center--xs">
                <a href="contacts.php" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50 g-padding-x-70--xs">Hire Us</a>
            </div>
        </div>
    </div>
    <!-- End Process -->
    <!--========== END PAGE CONTENT ==========-->
    <!--========== FOOTER ==========-->
    <?php include_once("footer.php") ?>
    <!--========== END FOOTER ==========-->
    <!-- Back To Top -->
    <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
    <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
    <!-- Vendor -->
    <script type="text/javascript" src="vendor/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
    <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
    <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>
    <!-- General Components and Settings -->
    <script type="text/javascript" src="js/global.min.js"></script>
    <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
    <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
    <script type="text/javascript" src="js/components/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/components/swiper.min.js"></script>
    <script type="text/javascript" src="js/components/wow.min.js"></script>
    <!--========== END JAVASCRIPTS ==========-->
</body>
<!-- End Body -->

</html>
