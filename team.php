<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Begin Head -->

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">
    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
    <!-- Vendor Styles -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css" />
    <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- Theme Styles -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/global/global.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
</head>
<!-- End Head -->
<!-- Body -->

<body>
    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->
    <!--========== PROMO BLOCK ==========-->
    <div class="g-bg-position--center js__parallax-window" style="background: url(img/1920x1080/09.jpg) 50% 0 no-repeat fixed;">
        <div class="g-container--md g-text-center--xs g-padding-y-150--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Combined 15+ years of experience</p>
            <h1 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white g-letter-spacing--1">Creative Team</h1>
        </div>
    </div>
    <!--========== END PROMO BLOCK ==========-->
    <!--========== PAGE CONTENT ==========-->
    <!-- Speakers -->
    <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="row g-overflow--hidden">
            <div class="col-xs-6 g-full-width--xs g-margin-b-30--xs g-margin-b-0--lg">
                <!-- Speaker -->
                <div class="center-block g-box-shadow__dark-lightest-v1 g-width-100-percent--xs g-width-400--lg">
                    <img class="img-responsive g-width-100-percent--xs" src="img/team/c_pro_1.png" alt="Image">
                    <div class="g-position--overlay g-padding-x-30--xs g-padding-y-30--xs g-margin-t-o-60--xs">
                        <div class="g-bg-color--primary g-padding-x-15--xs g-padding-y-10--xs g-margin-b-20--xs">
                            <h4 class="g-font-size-22--xs g-font-size-26--sm g-color--white g-margin-b-0--xs">John Doe</h4>
                        </div>
                        <p class="g-font-weight--700">CEO</p>
                        <p>Expertise in supply chain management (vendor development, sourcing & logistics)</p>
                    </div>
                </div>
                <!-- End Speaker -->
            </div>
            <div class="col-xs-6 g-full-width--xs">
                <!-- Speaker -->
                <div class="center-block g-box-shadow__dark-lightest-v1 g-width-100-percent--xs g-width-400--lg">
                    <img class="img-responsive g-width-100-percent--xs" src="img/team/v_pro_1.png" alt="Image">
                    <div class="g-position--overlay g-padding-x-30--xs g-padding-y-30--xs g-margin-t-o-60--xs">
                        <div class="g-bg-color--primary g-padding-x-15--xs g-padding-y-10--xs g-margin-b-20--xs">
                            <h4 class="g-font-size-22--xs g-font-size-26--sm g-color--white g-margin-b-0--xs">Jason Doe</h4>
                        </div>
                        <p class="g-font-weight--700">Business Development Head</p>
                        <p>Communication is his strong suite, backed with MBA in marketing(Lover of literature & Anime)</p>
                    </div>
                </div>
                <!-- End Speaker -->
            </div>
            <div class="col-xs-12 g-full-width--xs">
                <!-- Speaker -->
                <div class="center-block g-box-shadow__dark-lightest-v1 g-width-100-percent--xs g-width-400--lg">
                    <img class="img-responsive g-width-100-percent--xs" src="img/team/p_pro_1.png" alt="Image">
                    <div class="g-position--overlay g-padding-x-30--xs g-padding-y-30--xs g-margin-t-o-60--xs">
                        <div class="g-bg-color--primary g-padding-x-15--xs g-padding-y-10--xs g-margin-b-20--xs">
                            <h4 class="g-font-size-22--xs g-font-size-26--sm g-color--white g-margin-b-0--xs">Jack Doe</h4>
                        </div>
                        <p class="g-font-weight--700">CTO</p>
                        <p>6+ years experience in software development in Desktop, mobile & web (Loves to blog & travel)</p>
                    </div>
                </div>
                <!-- End Speaker -->
            </div>
        </div>
    </div>
    <!-- End Speakers -->
    <!-- Feedback Form -->
    <div class="g-position--relative g-bg-color--dark-light">
        <div class="g-container--md g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-80--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Contact Us</p>
                <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">Get in Touch</h2>
            </div>
            <div class="row g-row-col--5 g-margin-b-80--xs">
                <div class="col-xs-4 g-full-width--xs g-margin-b-50--xs g-margin-b-0--sm">
                    <div class="g-text-center--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--white-opacity g-margin-b-30--xs ti-email"></i>
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Email</h4>
                        <p class="g-color--white-opacity">business@casfertechnologies.com</p>
                    </div>
                </div>
                <div class="col-xs-4 g-full-width--xs g-margin-b-50--xs g-margin-b-0--sm">
                    <div class="g-text-center--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--white-opacity g-margin-b-30--xs ti-map-alt"></i>
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Address</h4>
                        <p class="g-color--white-opacity">Veroda, Cuncolim 
                        Salcete, Goa 
                        INDIA </p>
                    </div>
                </div>
                <div class="col-xs-4 g-full-width--xs">
                    <div class="g-text-center--xs">
                        <i class="g-display-block--xs g-font-size-40--xs g-color--white-opacity g-margin-b-30--xs ti-headphone-alt"></i>
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Call at</h4>
                        <p class="g-color--white-opacity">
                            <a href="tel:+918408881080">+91 8408881080</a><br>
                            <a href="tel:+917719855705">+91 7719855705</a><br>
                            <a href="tel:+917387352248">+91 7387352248</a><br>
                        </p>
                    </div>
                </div>
            </div>
            <form class="center-block g-width-500--sm g-width-550--md">
                <div class="g-margin-b-30--xs">
                    <input type="text" class="form-control s-form-v3__input" placeholder="* Name" id="name">
                </div>
                <div class="row g-row-col-5 g-margin-b-50--xs">
                    <div class="col-sm-6 g-margin-b-30--xs g-margin-b-0--md">
                        <input type="email" class="form-control s-form-v3__input" placeholder="* Email" id="email">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control s-form-v3__input" placeholder="* Phone" id="phone">
                    </div>
                </div>
                <div class="g-margin-b-80--xs">
                    <textarea class="form-control s-form-v3__input" rows="5" placeholder="* Your message" id="message"></textarea>
                </div>
                <div class="g-text-center--xs">
                    <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--white-bg g-radius--50 g-padding-x-70--xs g-margin-b-20--xs" id="btnContactUs">Submit</button>
                </div>
            </form>
        </div>
        <img class="s-mockup-v2" src="img/mockups/pencil-01.png" alt="Mockup Image">
    </div>
    <!-- End Feedback Form -->
    <!--========== END PAGE CONTENT ==========-->
    <!--========== FOOTER ==========-->
    <?php include_once("footer.php") ?>
    <!--========== END FOOTER ==========-->
    <!-- Back To Top -->
    <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
    <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
    <!-- Vendor -->
    <script type="text/javascript" src="vendor/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
    <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>
    <script type="text/javascript" src="vendor/sweetalert/js/sweetalert.min.js"></script>
    <!-- General Components and Settings -->
    <script type="text/javascript" src="js/global.min.js"></script>
    <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
    <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
    <script type="text/javascript" src="js/components/wow.min.js"></script>
    <script type="text/javascript" src="js/mails-sendgrid.js"></script>
    <!--========== END JAVASCRIPTS ==========-->
</body>
<!-- End Body -->

</html>
