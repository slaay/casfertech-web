$(document).ready(function() {
    $("#btnContactUs").on("click", "", function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var message = $("#message").val();
        
        $.ajax({
            type: "POST",
            url: "vendor/sendgrid/contactusMail.php",
            data: { 'name': name, 'email': email, 'message': message, 'phone': phone },
            cache: false,
            success: function() {
                $('#name').val("");
                $('#email').val("");
                $('#message').val("");
                $('#phone').val("");
                swal("Thank you! We will be in contact soon!");
            }
        });
        event.preventDefault();
    });

    $("#btnSoftwareRequirement").on("click", "", function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var message = $("#requirement").val();
        
        $.ajax({
            type: "POST",
            url: "vendor/sendgrid/contactusMail.php",
            data: { 'name': name, 'email': email, 'message': message, 'phone': phone },
            cache: false,
            success: function() {
                $('#name').val("");
                $('#email').val("");
                $('#requirement').val("");
                $('#phone').val("");
                swal("Thank you! We will be in contact soon!");
            }
        });
        event.preventDefault();
    });

    $("#btnBrochure").on("click", "", function() {
        var email = $("#emailbrochure").val();
       
        $.ajax({
            type: "POST",
            url: "vendor/sendgrid/brochureMail.php",
            data: { 'email': email},
            cache: false,
            success: function() {
                $('#emailbrochure').val("");
                swal("Thank you! We will be in contact soon!");
            }
        });
        event.preventDefault();
    });

    $("form[name='upload_img']").submit(function(e) {
        var customerName = $("#customerName").val();
        var customerPhone = $("#customerPhone").val();
        var customerEmail = $("#customerEmail").val();
        var customerParts = $("#customerParts").val();
        var customerAdress = $("#customerAdress").val();
        var customerReference = $("#customerReference").val();

        if (($.trim(customerName).length < 1) ||  ($.trim(customerPhone).length < 1) ||
            ($.trim(customerEmail).length < 1) ||  ($.trim(customerParts).length < 1) || 
            ($.trim(customerAdress).length < 1)) {
                swal("Please fill in all the required fields!");
                return;
            }

        var formData = new FormData($(this)[0]);
        formData.append('customerName', customerName);
        formData.append('customerPhone', customerPhone);
        formData.append('customerAdress', customerAdress);
        formData.append('customerEmail', customerEmail);
        formData.append('customerParts', customerParts);
        formData.append('customerReference', customerReference);

        var file = document.getElementById('image_file').files[0];
        if (file) {
            formData.append('image_file', file);
        }

        console.log("formdata : ", JSON.stringify(formData))
        $.ajax({
            url: "sendgrid/enquiryMail.php",
            type: "POST",
            data: formData,
            async: false,
            success: function(msg) {
                $('#customerName').val("");
                $('#customerPhone').val("");
                $('#customerEmail').val("");
                $('#customerParts').val("");
                $("#customerAdress").val("");
                $("#customerReference").val("");
                swal("Thank you! We will be in contact soon!");
            },
            cache: false,
            contentType: false,
            processData: false
        });

        e.preventDefault();
    });
});
