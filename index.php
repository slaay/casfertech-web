<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Begin Head -->

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">
    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
    <!-- Vendor Styles -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css" />
    <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- Theme Styles -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/global/global.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
</head>
<!-- End Head -->
<!-- Body -->

<body>
    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->
    <!--========== SWIPER SLIDER ==========-->
    <div class="s-swiper js__swiper-one-item">
        <!-- Swiper Wrapper -->
        <div class="swiper-wrapper">
            <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/02.jpg');">
                <div class="container g-text-center--xs g-ver-center--xs">
                    <div class="g-margin-b-30--xs">
                        <h1 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Undistrupting supply chain<br> through technology</h1>
                    </div>
                    <a class="js__popup__youtube" href="https://www.youtube.com/watch?v=prDJO1HKoq8" title="Intro Video">
                        <i class="s-icon s-icon--lg s-icon--white-bg g-radius--circle ti-control-play"></i>
                    </a>
                </div>
            </div>
            <div class="g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/01.jpg');">
                <div class="container g-text-center--xs g-ver-center--xs">
                    <div class="g-margin-b-30--xs">
                        <div class="g-margin-b-30--xs">
                            <h2 class="g-font-size-35--xs g-font-size-45--sm g-font-size-55--md g-color--white">Quality software<br>that defines you!</h2>
                        </div>
                        <a class="js__popup__youtube" href="https://www.youtube.com/watch?v=prDJO1HKoq8" title="Intro Video">
                            <i class="s-icon s-icon--lg s-icon--white-bg g-radius--circle ti-control-play"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Swiper Wrapper -->
        <!-- Arrows -->
        <a href="javascript:void(0);" class="s-swiper__arrow-v1--right s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-right js__swiper-btn--next"></a>
        <a href="javascript:void(0);" class="s-swiper__arrow-v1--left s-icon s-icon--md s-icon--white-brd g-radius--circle ti-angle-left js__swiper-btn--prev"></a>
        <!-- End Arrows -->
        <a href="#js__scroll-to-section" class="s-scroll-to-section-v1--bc g-margin-b-15--xs">
            <span class="g-font-size-18--xs g-color--white ti-angle-double-down"></span>
            <p class="text-uppercase g-color--white g-letter-spacing--3 g-margin-b-0--xs">Learn More</p>
        </a>
    </div>
    <!--========== END SWIPER SLIDER ==========-->
    <!--========== PAGE CONTENT ==========-->
    <!-- Features -->
    <div id="js__scroll-to-section" class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-text-center--xs g-margin-b-100--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Welcome to Casfer Technologies</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md">We Create Beautiful Experiences <br> That Drive Successful Businesses.</h2>
        </div>
  
        <ul id="rig">
            <li>
                <a class="rig-cell" href="freightrates.php">
                    <img class="rig-img" src="img/services/freightrates-main.jpg">
                    <span class="rig-overlay"></span>
                    <span class="rig-text">FreightRates</span>
                </a>
            </li>
            <li>
                <a class="rig-cell" href="software_services.php">
                    <img class="rig-img" src="img/services/software-main.jpg">
                    <span class="rig-overlay"></span>
                    <span class="rig-text">Software Development</span>
                </a>
            </li>
            <li>
                <a class="rig-cell" href="sourcing_services.php">
                <img class="rig-img" src="img/services/sourcing-main.jpg">
                    <span class="rig-overlay"></span>
                    <span class="rig-text">Sourcing</span>
                </a>
            </li>
            <li>
                <a class="rig-cell" href="sourcing_services.php">
                <img class="rig-img" src="img/services/ecommerce-main.jpg">
                    <span class="rig-overlay"></span>
                    <span class="rig-text">E commerce</span>
                </a>
            </li>
        </ul>
    </div>  

    
    <!-- End Features -->
    <!-- Parallax -->
    <div class="js__parallax-window" style="background: url(img/1920x1080/03.jpg) 50% 0 no-repeat fixed;">
        <div class="container g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-margin-b-80--xs">
                <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white">The best way to source products</h2>
            </div>
            <a href="#" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50">Learn More</a>
        </div>
    </div>
    <!-- End Parallax -->
    <!-- Culture -->
    <div class="g-promo-section">
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="row">
                <div class="col-md-4 g-margin-t-15--xs g-margin-b-60--xs g-margin-b-0--lg">
                    <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Story</p>
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                        <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md">About</h2>
                    </div>
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                        <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md">Casfer Tech</h2>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <p class="g-font-size-18--xs">On this planet, in the Asian continent, in western India of the southern part of Goa were three young men. Lived a modest life until a strike of an idea brought them together. From 9 am to 6 pm they are simple, regular guys with normal life but from 6.01 pm they are saving the world and Un-disrupting supply chains through Technology.</p>
                    <p class="g-font-size-18--xs">We work. Mostly because it pays and because it is fun. When the tangle of unclear roads jumps at our client’s Destination..<a href="about.php#js__scroll-to-section" class="text-uppercase s-btn s-btn--md g-radius--50">More about us</a></p>
                </div>
            </div>
        </div>
        <div class="col-sm-3 g-promo-section__img-right--lg g-bg-position--center g-height-100-percent--md js__fullwidth-img">
            <img class="img-responsive" src="img/970x970/03.jpg" alt="Image">
        </div>
    </div>
    <!-- End Culture -->
    <!-- Subscribe -->
    <div class="js__parallax-window" style="background: url(img/1920x1080/07.jpg) 50% 0 no-repeat fixed;">
        <div class="g-container--sm g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-margin-b-80--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Get to know us</p>
                <h2 class="g-font-size-32--xs g-font-size-36--md g-color--white">We are just an email away</h2>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                    <form class="input-group">
                        <input type="email" class="form-control s-form-v1__input g-radius--left-50" name="email" id="emailbrochure" placeholder="Enter your email">
                        <span class="input-group-btn">
                                <button type="submit" class="s-btn s-btn-icon--md s-btn-icon--white-brd s-btn--white-brd g-radius--right-50" id="btnBrochure"><i class="ti-arrow-right"></i></button>
                            </span>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Subscribe -->
    <!-- Portfolio Filter -->
    <div class="container g-padding-y-80--xs">
        <div class="g-text-center--xs g-margin-b-40--xs">
            <h2 class="g-font-size-32--xs g-font-size-36--md">Projects</h2>
        </div>
        <div class="s-portfolio">
            <div id="js__filters-portfolio-gallery" class="s-portfolio__filter-v1 cbp-l-filters-text cbp-l-filters-center">
                <div data-filter="*" class="s-portfolio__filter-v1-item cbp-filter-item cbp-filter-item-active">Show All</div>
                <div data-filter=".software" class="s-portfolio__filter-v1-item cbp-filter-item">Software</div>
                <div data-filter=".sourcing" class="s-portfolio__filter-v1-item cbp-filter-item">Sourcing</div>
                <div data-filter=".services" class="s-portfolio__filter-v1-item cbp-filter-item">Services sourcing</div>
            </div>
        </div>
    </div>
    <!-- End Portfolio Filter -->
    <!-- Portfolio Gallery -->
    <div class="container g-margin-b-100--xs">
        <div id="js__grid-portfolio-gallery" class="cbp">
            <!-- Item -->
            <div class="s-portfolio__item cbp-item software">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/freightates.png" alt="FreightRates">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">FreightRates</h4>
                        <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/freightates.png" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="FreightRates <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="freightrates.php" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->
            <!-- Item -->
            <div class="s-portfolio__item cbp-item software">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/drivinghorrors.png" alt="DrivingHorrors">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">DrivingHorrors</h4>
                        <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/drivinghorrors.png" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="DrivingHorrors <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://drivinghorrors.com/" target="_blank" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->

            <!-- Item -->
            <div class="s-portfolio__item cbp-item software">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/empowering-goa.png" alt="Empowering Goa">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Empowering Goa</h4>
                        <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/empowering-goa.png" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Empowering Goa <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://empoweringgoans.com/" target="_blank" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->

            <div class="s-portfolio__item cbp-item sourcing">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/sourcing-wallets.jpg" alt="Travel accessories">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Travel accessories</h4>
                         <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/sourcing-wallets.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Travel accessories <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->
            <div class="s-portfolio__item cbp-item sourcing">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/sourcing-electronics.jpg" alt="Electronic items">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Electronic items</h4>
                         <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/sourcing-electronics.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Electronic items <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->
            <div class="s-portfolio__item cbp-item sourcing">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/sourcing-masks.jpg" alt="Masks">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Masks</h4>
                         <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/sourcing-masks.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Masks <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Item -->
            <div class="s-portfolio__item cbp-item services">
                <div class="s-portfolio__img-effect">
                    <img src="img/projects/sourcing-services.jpg" alt="Services">
                </div>
                <div class="s-portfolio__caption-hover--cc">
                    <div class="g-margin-b-25--xs">
                        <h4 class="g-font-size-18--xs g-color--white g-margin-b-5--xs">Services</h4>
                         <p class="g-color--white-opacity">Casfer Technologies.</p>
                    </div>
                    <ul class="list-inline g-ul-li-lr-5--xs g-margin-b-0--xs">
                        <li>
                            <a href="img/projects/sourcing-services.jpg" class="cbp-lightbox s-icon s-icon--sm s-icon--white-bg g-radius--circle" data-title="Services <br/> by Casfer Technologies.">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="s-icon s-icon--sm s-icon s-icon--white-bg g-radius--circle">
                                <i class="ti-link"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Portfolio Gallery -->
    </div>
    <!-- End Portfolio -->
     <!-- Testimonials -->
      <?php include_once("testimonial-partial.php") ?>
    <!-- End Testimonials -->
    <!-- Clients -->
    <div class="g-bg-color--sky-light">
        <div class="g-text-center--xs g-padding-y-20--xs g-margin-b-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Clients</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md">Happy clients make us happy</h2>
        </div>
        <div class="container g-padding-y-100--xs g-padding-y-125--sm">
            <div class="wow fadeIn" data-wow-duration=".3" data-wow-delay=".1s">
                <img src="img/clients/client-map.png" class="img-responsive" alt="Clients Logo">
            </div>
        </div>
    </div>
    <!-- End Clients  -->
    <!-- News -->
<!--     <div class="container g-padding-y-80--xs g-padding-y-125--sm">
        <div class="g-text-center--xs g-margin-b-80--xs">
            <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Blog</p>
            <h2 class="g-font-size-32--xs g-font-size-36--md">Latest News</h2>
        </div>
        <div class="row">
            <div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">
                
                <article>
                    <img class="img-responsive" src="img/970x970/01.jpg" alt="Image">
                    <div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>
                        <h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="http://keenthemes.com/">Create Something Great.</a></h3>
                        <p>The time has come to bring those ideas and plans to life.</p>
                    </div>
                </article>
                
            </div>
            <div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">
                
                <article>
                    <img class="img-responsive" src="img/970x970/02.jpg" alt="Image">
                    <div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>
                        <h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="http://keenthemes.com/">Jacks of All. Experts in All.</a></h3>
                        <p>The time has come to bring those ideas and plans to life.</p>
                    </div>
                </article>
                
            </div>
            <div class="col-sm-4">
               
                <article>
                    <img class="img-responsive" src="img/970x970/03.jpg" alt="Image">
                    <div class="g-bg-color--white g-box-shadow__dark-lightest-v2 g-text-center--xs g-padding-x-40--xs g-padding-y-40--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2">News</p>
                        <h3 class="g-font-size-22--xs g-letter-spacing--1"><a href="http://keenthemes.com/">Finding your Perfect Place.</a></h3>
                        <p>The time has come to bring those ideas and plans to life.</p>
                    </div>
                </article>
               
            </div>
        </div>
    </div> -->
    <!-- End News -->
    <!-- Counter -->
    <?php include_once("records-counter-partial.php") ?>
    <!-- End Counter -->
    <!-- Feedback Form -->
    <div class="g-bg-color--sky-light">
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="g-text-center--xs g-margin-b-80--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Feedback</p>
                <h2 class="g-font-size-32--xs g-font-size-36--md">Send us a note</h2>
            </div>
            <form>
                <div class="row g-margin-b-40--xs">
                    <div class="col-sm-6 g-margin-b-20--xs g-margin-b-0--md">
                        <div class="g-margin-b-20--xs">
                            <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Name" id="name">
                        </div>
                        <div class="g-margin-b-20--xs">
                            <input type="email" class="form-control s-form-v2__input g-radius--50" placeholder="* Email" id="email">
                        </div>
                        <input type="text" class="form-control s-form-v2__input g-radius--50" placeholder="* Phone" id="phone">
                    </div>
                    <div class="col-sm-6">
                        <textarea class="form-control s-form-v2__input g-radius--10 g-padding-y-20--xs" rows="8" placeholder="* Your message" id="message"></textarea>
                    </div>
                </div>
                <div class="g-text-center--xs">
                    <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-80--xs" id="btnContactUs">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End Feedback Form -->
    <!-- Google Map -->
    <section class="s-google-map">
        <div id="js__google-container" class="s-google-container g-height-400--xs"></div>
    </section>
    <!-- End Google Map -->
    <!--========== END PAGE CONTENT ==========-->
    <!--========== FOOTER ==========-->
    <?php include_once("footer.php") ?>
    <!--========== END FOOTER ==========-->
    <!-- Back To Top -->
    <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
    <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
    <!-- Vendor -->
    <script type="text/javascript" src="vendor/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
    <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
    <script type="text/javascript" src="vendor/waypoint.min.js"></script>
    <script type="text/javascript" src="vendor/counterup.min.js"></script>
    <script type="text/javascript" src="vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U"></script>
    <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>
    <script type="text/javascript" src="vendor/sweetalert/js/sweetalert.min.js"></script>
    <!-- General Components and Settings -->
    <script type="text/javascript" src="js/global.min.js"></script>
    <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
    <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
    <script type="text/javascript" src="js/components/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/components/swiper.min.js"></script>
    <script type="text/javascript" src="js/components/counter.min.js"></script>
    <script type="text/javascript" src="js/components/portfolio-3-col.min.js"></script>
    <script type="text/javascript" src="js/components/parallax.min.js"></script>
    <script type="text/javascript" src="js/components/google-map.js"></script>
    <script type="text/javascript" src="js/components/wow.min.js"></script>
    <script type="text/javascript" src="js/mails-sendgrid.js"></script>
    <!--========== END JAVASCRIPTS ==========-->
</body>
<!-- End Body -->

</html>
