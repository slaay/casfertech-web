<footer class="g-bg-color--dark">
    <!-- Links -->
    <div class="g-hor-divider__dashed--white-opacity-lightest">
        <div class="container g-padding-y-80--xs">
            <div class="row">
                <div class="col-sm-2 g-margin-b-20--xs g-margin-b-0--md">
                    <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="index.php">Home</a></li>
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="about.php">About</a></li>
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="contacts.php">Contact</a></li>
                    </ul>
                </div>
                <div class="col-sm-2 g-margin-b-20--xs g-margin-b-0--md">
                    <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="https://twitter.com/casfertech">Twitter</a></li>
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="https://www.facebook.com/casfertech">Facebook</a></li>
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="https://www.youtube.com/channel/UCSa-fqG0jFKNM37PtX5jDuw">YouTube</a></li>
                    </ul>
                </div>
                <div class="col-sm-2 g-margin-b-40--xs g-margin-b-0--md">
                    <ul class="list-unstyled g-ul-li-tb-5--xs g-margin-b-0--xs">
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="include/privacypolicy.pdf" target="_blank">Privacy Policy</a></li>
                        <li><a class="g-font-size-15--xs g-color--white-opacity" href="include/termsofuse.pdf" target="_blank">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 s-footer__logo g-padding-y-50--xs g-padding-y-0--md">
                    <h3 class="g-font-size-18--xs g-color--white">Casfer Technologies</h3>
                    <p class="g-color--white-opacity">Undistrupting supply chain
                        <br> through technology</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Links -->
    <!-- Copyright -->
    <div class="container g-padding-y-50--xs">
        <div class="row">
            <div class="col-xs-6 g-text-right--xs">
                <p class="g-font-size-14--xs g-margin-b-0--xs g-color--white-opacity-light">© CasFer Technologies Pvt. Ltd. (OPC) All Rights Reserved. Powered by: <a href="#">CasferTechnologies.com</a></p>
            </div>
        </div>
    </div>
    <!-- End Copyright -->
</footer>
