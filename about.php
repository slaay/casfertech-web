<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- Begin Head -->

<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Casfer Technologies</title>
    <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
    <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
    <meta name="author" content="CasFer Technologies">
    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">
    <!-- Vendor Styles -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.css" rel="stylesheet" type="text/css" />
    <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css" />
    <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css" />
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme Styles -->
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/global/global.css" rel="stylesheet" type="text/css" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
</head>
<!-- End Head -->
<!-- Body -->

<body>
    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->
    <!--========== PROMO BLOCK ==========-->
    <div class="g-fullheight--md js__parallax-window" style="background: url(img/1920x1080/02.jpg) 50% 0 no-repeat fixed;">
        <div class="g-container--md g-text-center--xs g-ver-center--md g-padding-y-150--xs g-padding-y-0--md">
            <div class="g-margin-b-60--xs">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--white-opacity g-letter-spacing--2 g-margin-b-25--xs">Welcome to Casfer Technologies</p>
                <h1 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white g-letter-spacing--1">Creative Design</h1>
                <p class="g-font-size-18--xs g-font-size-26--md g-color--white-opacity g-margin-b-0--xs">We Create Beautiful Experiences
                    <br> That Drive Successful Businesses.</p>
            </div>
            <span>
             <br/>
             <br/>
             <br/>
            </span>
            <a href="#js__scroll-to-section" class="s-scroll-to-section-v1--bc g-margin-b-15--xs">
            <span class="g-font-size-18--xs g-color--white ti-angle-double-down"></span>
            <p class="text-uppercase g-color--white g-letter-spacing--3 g-margin-b-0--xs">Learn More</p>
        </a>
        </div>
    </div>
    <!--========== END PROMO BLOCK ==========-->
    <!--========== PAGE CONTENT ==========-->
    <!-- About -->
    <div class="container g-padding-y-80--xs g-padding-y-125--sm" id="js__scroll-to-section" >
        <div class="row g-hor-centered-row--md g-row-col--5 g-margin-b-60--xs g-margin-b-100--md">
            <div class="col-sm-3 col-xs-6 g-hor-centered-row__col">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                    <img class="img-responsive" src="img/600x800/casfer.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 g-hor-centered-row__col g-margin-b-60--xs g-margin-b-0--md">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                    <img class="img-responsive" src="img/600x800/tech.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 g-hor-centered-row__col">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">The story</p>
                <p class="g-font-size-18--sm">On this planet, in the Asian continent, in western India of the southern part of Goa were three young men. Lived a modest life until a strike of an idea brought them together. From 9 pm to 6 am there are simple, regular guys with normal life but from 6.01 am they are saving the world and Un-disrupting supply chains through Technology.</p>
            </div>
        </div>
        <div class="row g-hor-centered-row--md g-row-col--5">
            <div class="col-sm-3 col-xs-6 col-sm-push-6 g-hor-centered-row__col">
                <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".2s">
                    <img class="img-responsive" src="img/600x800/truck.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 col-sm-push-6 g-hor-centered-row__col g-margin-b-60--xs g-margin-b-0--md">
                <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".1s">
                    <img class="img-responsive" src="img/600x800/package.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 col-sm-pull-7 g-hor-centered-row__col g-text-left--xs g-text-right--md">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">What do we do?</p>
                <p class="g-font-size-18--sm">We work. Mostly because it pays and because it is fun. When the tangle of unclear roads jumps at our client’s Destination we usually provide a Jet pack. Or sometimes when it is more difficult to find a manufacturer or a wholesaler for a Retailer (product sourcer) than a Needle in a haystack we implement what Iron man said – we get a big magnet of Researchers. There are a lot of fun stuff we do that you can also be part of. Why not write us (<a href="contacts.php"> contact us!</a>), and we promise we will get back to you by 24 hours, if we are not super busy saving the world.</p>
            </div>
        </div>
        <div class="row g-hor-centered-row--md g-row-col--5 g-margin-b-60--xs g-margin-b-100--md">
            <div class="col-sm-3 col-xs-6 g-hor-centered-row__col">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".1s">
                    <img class="img-responsive" src="img/600x800/apple.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 g-hor-centered-row__col g-margin-b-60--xs g-margin-b-0--md">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".2s">
                    <img class="img-responsive" src="img/600x800/world.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 g-hor-centered-row__col">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Casual Day at Work</p>
                <p class="g-font-size-18--sm">With our suits and invisible capes hanging, flying from our backs with our charged-up finger tips on Super computers. Diving into the mail box to save the drowning ships and saving those by giving a solution of a floater. Then, flying to the various places through conference calls on various issues; Feasibility studies, Consultation, Product Research and Sourcing, Logistics, Software Development and most of the times the neighbour’s cat from the tree. By evening we are fighting our arch nemeses Time to complete the work and we defeat him every single time. (it’s funny, get it?).</p>
            </div>
        </div>
        <div class="row g-hor-centered-row--md g-row-col--5">
            <div class="col-sm-3 col-xs-6 col-sm-push-6 g-hor-centered-row__col">
                <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".2s">
                    <img class="img-responsive" src="img/600x800/rocket.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 col-sm-push-6 g-hor-centered-row__col g-margin-b-60--xs g-margin-b-0--md">
                <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".1s">
                    <img class="img-responsive" src="img/600x800/cup.jpg" alt="Image">
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5 col-sm-pull-7 g-hor-centered-row__col g-text-left--xs g-text-right--md">
                <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-25--xs">Super powers</p>
                <p class="g-font-size-18--sm"><strong>Experience Unity</strong> <br>
                 Our experience climbs mountain of a decade – in Software Development, e commerce, supply chain management and logistics. Charmingly Good Looking If you get in a conference call with us we could take off our superhero mask to show you how devilishly handsome and charming, we are. Also, greatly, equipped with Problem Solving and Consultations skills. Humble That’s not true! We are super heroes for crying out loud. Unless you see yourself as a superhero we can sit across a table. And shsss! Since you found us, you are SPEACIAL.</p>
            </div>
        </div>
    </div>
    <!-- End About -->
    <!--========== END PAGE CONTENT ==========-->
    <!--========== FOOTER ==========-->
    <?php include_once("footer.php") ?>
    <!--========== END FOOTER ==========-->
    <!-- Back To Top -->
    <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>
    <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
    <!-- Vendor -->
    <script type="text/javascript" src="vendor/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
    <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
    <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
    <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>
    <!-- General Components and Settings -->
    <script type="text/javascript" src="js/global.min.js"></script>
    <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
    <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
    <script type="text/javascript" src="js/components/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/components/swiper.min.js"></script>
    <script type="text/javascript" src="js/components/wow.min.js"></script>
    <!--========== END JAVASCRIPTS ==========-->
</body>
<!-- End Body -->

</html>
