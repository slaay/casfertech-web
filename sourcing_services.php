<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- Begin Head -->
    <head>
        <!-- Basic -->
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Casfer Technologies</title>
        <meta name="keywords" content="SUPPLY CHAIN , LOGISTICS , PROCUREMENT ,SOURCING ,ECOMMERCE , NEW PRODUCT DEVELOPMENT,MANUFACTURING,VENDOR DEVELOPMENT,SUPPLIERS , DEMAND ,SUPPLY ,FREIGHT RATES ,FREIGHT , PACKAGE ,DELIVERY ,ON TIME , DISTRIBUTOR , CATEGORY , AIR , SEA , ROAD , CARRIER , FREIGHT FORWARDER , LCD SCREENS, LED SCREENS, WALLET, STEEL, ALUMINIUM, PROFIT MARGIN , INVENTORY MANAGEMENT , SPEND MANAGEMENT , BOTTOM LINE , PROFITABILITY , SERVICES SOURCING , PRODUCT SOURCING ,SOFTWARE DEVELOPMENT , JAVA , ANGULAR JS , WEBSITE DEVELOPMENT , TONNES , KG , CUBIC METRE , WEIGHTS ,  VOLUME , VENDOR EVALUATION , ALIBABA SOURCING ,  MADE IN CHINA , AMAZON FBA SERVICES , EBAY SERVICES , DROP SHIPPING , CHINA SOURCING , INDIA SOURCING ,PROTOTYPE MANUFACTURING , RETAIL PRODUCT SOURCING , UPWORK SOURCING , FREELANCER , OPTIMIZING INVENTORY, ANDROID APP DEVELOPMENT, IOS APP DEVELOPMENT , LAPTOP PARTS SOURCING , CONTRACT MANUFACTURERS ,PRODUCT RESEARCHER , FREIGHTRATES.IN , E-WASTE SOURCING , EXPORTER , WEB DESIGN , SOURCING SUPPORT , VENDOR MANAGEMENT , RISK ASSESMENT FOR VENDORS , VENDOR RATING , STRATERGIC SOURCING , SOURCING PLATFORM , VENDOR NEGOTIAIONS , VENDOR EVALUATION , MAKE IN INDIA ,  GLOBAL SUPPLY CHAIN , BULK SOURCING" />
        <meta name="description" content="CASFER TECHNOLOGIES – UNDISRUPTING SUPPLY CHAINS THROUGH TECHNOLOGY!" />
        <meta name="author" content="CasFer Technologies">

        <!-- Web Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i|Montserrat:400,700" rel="stylesheet">

        <!-- Vendor Styles -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/themify/themify.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/scrollbar/scrollbar.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/swiper/swiper.min.css" rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link href="css/global/global.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    </head>
    <!-- End Head -->

    <!-- Body -->
    <body>

    <!--========== HEADER ==========-->
    <?php include_once("analytics.php") ?>
    <?php include_once("header.php") ?>
    <!--========== END HEADER ==========-->

        <!--========== SWIPER SLIDER ==========-->
        <div class="s-swiper js__swiper-slider">
            <!-- Swiper Wrapper -->
            <div class="swiper-wrapper">
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/sourcing_Services_industrial.jpg');">
                    <div class="container g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="g-margin-b-50--xs">
                                    <h1 class="g-font-size-32--xs g-font-size-45--sm g-font-size-60--md g-color--white">Industrial Sourcing</h1>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">
                                        The best vendors with competitive pricing.
                                    </p>
                                </div>
                                <a href="#scroll_to_industrial" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/sourcing_Services_e-commerce.jpg');">
                    <div class="container g-text-right--xs g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="g-margin-b-50--xs">
                                    <h2 class="g-font-size-32--xs g-font-size-45--sm g-font-size-55--md g-color--white">E-commerce Sourcing</h2>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">The best vendors with competitive pricing.</p>
                                </div>
                                <a href="#scroll_to_ecommerce" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="s-promo-block-v4 g-fullheight--xs g-bg-position--center swiper-slide" style="background: url('img/1920x1080/sourcing_Services_services.jpg');">
                    <div class="container g-text-right--xs g-ver-center--xs">
                        <div class="row">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="g-margin-b-50--xs">
                                    <h2 class="g-font-size-32--xs g-font-size-45--sm g-font-size-55--md g-color--white">Services Sourcing</h2>
                                    <p class="g-font-size-18--xs g-font-size-22--sm g-color--white-opacity">Helping you serve customers efficiently.</p>
                                </div>
                                <a href="#scroll_to_services" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50 g-padding-x-50--xs">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Swiper Wrapper -->

            <!-- Pagination -->
            <div class="s-swiper__pagination-v1 s-swiper__pagination-v1--bc s-swiper__pagination-v1--white js__swiper-pagination"></div>
        </div>
        <!--========== END SWIPER SLIDER ==========-->

        <!--========== PAGE CONTENT ==========-->
        <!--Industrial Services -->
        <div class="container g-padding-y-80--xs g-padding-y-125--sm" id="scroll_to_industrial">
            <div class="row g-margin-b-10--xs">
                <div class="col-md-6 g-margin-b-60--xs g-margin-b-0--lg">
                    <!-- Masonry Grid -->
                    <div class="row g-row-col--5 g-overflow--hidden js__masonry">
                        <div class="col-xs-6 js__masonry-sizer"></div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs g-margin-b-0--sm js__masonry-item">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="img-responsive" src="img/services/industrial-sourcing-1.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".3s">
                                <img class="img-responsive" src="img/services/industrial-sourcing-2.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".5s">
                                <img class="img-responsive" src="img/services/industrial-sourcing-3.jpg" alt="Image">
                            </div>
                        </div>
                    </div>
                    <!-- End Masonry Grid -->
                </div>
                <div class="col-md-5 g-margin-b-10--xs g-margin-b-0--lg g-margin-t-10--lg g-margin-l-20--lg" id="js__scroll-to-section">
                    <div class="g-margin-b-30--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-15--xs">Industrial</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--sm">We Specialize In</h2>
                        <p>The best vendors with competitive pricing.</p>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled col-xs-12 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Locate the best manufacturing facilities for your products.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Reduce the cost of manufacturing and improve bottom lines (profits).</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Detailed evaluation matrix to find the best vendor.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>We negotiate and finalize the best price for your product requirements.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>We assist in maintaining quality, schedule inspection and maintain standards as per set frame work.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--E - Commerce Services -->
        <div class="container g-padding-y-80--xs g-padding-y-125--sm" id="scroll_to_ecommerce">
            <div class="row g-margin-b-10--xs">
                <div class="col-md-6 g-margin-b-60--xs g-margin-b-0--lg">
                    <!-- Masonry Grid -->
                    <div class="row g-row-col--5 g-overflow--hidden js__masonry">
                        <div class="col-xs-6 js__masonry-sizer"></div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs g-margin-b-0--sm js__masonry-item">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="img-responsive" src="img/services/e-commerce-1.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".3s">
                                <img class="img-responsive" src="img/services/e-commerce-3.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-xs-6 g-full-width--xs js__masonry-item">
                            <div class="wow fadeInRight" data-wow-duration=".3" data-wow-delay=".5s">
                                <img class="img-responsive" src="img/services/e-commerce-2.jpg" alt="Image">
                            </div>
                        </div>
                    </div>
                    <!-- End Masonry Grid -->
                </div>
                <div class="col-md-5 g-margin-b-10--xs g-margin-b-0--lg g-margin-t-10--lg g-margin-l-20--lg" id="js__scroll-to-section">
                    <div class="g-margin-b-30--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-15--xs">E-Commerce</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--sm">We Specialize In</h2>
                        <p>The best vendors with competitive pricing.</p>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled col-xs-12 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Find trending products with our research expertise and source from the best vendors to provide the best deals across E-commerce platforms.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>We assist in building your product portfolios across platforms.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Find manufacturers and distributors for made to stock items at competitive rates.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Evaluate and negotiate the best deals.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Finalize orders for shipment.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Purchase and inventory management solutions.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

       <!--Services Services -->
        <div class="container g-padding-y-80--xs g-padding-y-125--sm" id="scroll_to_services">
            <div class="row g-margin-b-10--xs">
                <div class="col-md-6 g-margin-b-60--xs g-margin-b-0--lg">
                    <!-- Masonry Grid -->
                    <div class="row g-row-col--5 g-overflow--hidden js__masonry">
                        <div class="col-xs-6 js__masonry-sizer"></div>
                        <div class="col-xs-6 g-full-width--xs g-margin-b-10--xs g-margin-b-0--sm js__masonry-item">
                            <div class="wow fadeInDown" data-wow-duration=".3" data-wow-delay=".1s">
                                <img class="img-responsive" src="img/services/services-sourcing-1.jpg" alt="Image">
                            </div>
                        </div>

                    </div>
                    <!-- End Masonry Grid -->
                </div>
                <div class="col-md-5 g-margin-b-10--xs g-margin-b-0--lg g-margin-t-10--lg g-margin-l-20--lg" id="js__scroll-to-section">
                    <div class="g-margin-b-30--xs">
                        <p class="text-uppercase g-font-size-14--xs g-font-weight--700 g-color--primary g-letter-spacing--2 g-margin-b-15--xs">Services</p>
                        <h2 class="g-font-size-32--xs g-font-size-36--sm">We Specialize In</h2>
                        <p>Helping you serve customers efficiently.</p>
                    </div>
                    <div class="row">
                        <ul class="list-unstyled col-xs-12 g-full-width--xs g-ul-li-tb-5--xs g-margin-b-20--xs g-margin-b-0--sm">
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Focus on your core competencies and outsource functions for maximizing value.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>We assists in finding the best outsourcing firms.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Detailed and customized evaluation matrix tailored to identify and evaluate firms in line with your requirements.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Float RFP to find the best fit.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Evaluate and negotiate on your behalf.</li>
                            <li><i class="g-font-size-12--xs g-color--primary g-margin-r-10--xs ti-check"></i>Setup contracts, NDAs & finalise deals.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Services -->

        <!-- Parallax -->
        <div class="s-promo-block-v5 g-bg-position--center js__parallax-window" style="background: url(img/1920x1080/software_Services_background_3.jpg) 50% 0 no-repeat fixed;">
            <div class="container g-text-center--xs g-padding-y-80--xs g-padding-y-125--sm">
                <div class="g-margin-b-80--xs">
                    <h2 class="g-font-size-40--xs g-font-size-50--sm g-font-size-60--md g-color--white">Looking for sourcing plans?</h2>
                </div>
                <a href="sourcing-plans.php" class="text-uppercase s-btn s-btn--md s-btn--white-brd g-radius--50">Learn More</a>
            </div>
        </div>
        <!-- End Parallax -->



        <!-- Form -->
        <div id="js__scroll-to-appointment" class="g-bg-color--sky-light g-padding-y-80--xs g-padding-y-125--sm">
            <div class="container g-bg-color--white g-box-shadow__dark-lightest-v3">
                <div class="row">
                    <!-- Form -->
                    <div class="col-md-8 js__form-eqaul-height-v1">
                        <div class="g-padding-x-40--xs g-padding-y-50--xs">
                            <h2 class="g-font-size-24--xs g-color--primary g-margin-b-50--xs">Get a free quote</h2>
                            <form>
                                <div class="row g-margin-b-30--xs g-margin-b-50--md">
                                    <div class="col-sm-6 g-margin-b-30--xs g-margin-b-0--md">
                                        <input type="text" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Full Name" id="name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Phone Number" id="phone">
                                    </div>
                                </div>
                                <div class="row g-margin-b-50--xs g-margin-b-50--md">
                                    <div class="col-sm-4 g-margin-b-30--xs g-margin-b-0--md">
                                        <input type="email" class="form-control s-form-v4__input g-padding-l-0--xs" placeholder="* Email" id="email">
                                    </div>
                                </div>
                                <div class="g-margin-b-50--xs">
                                    <textarea class="form-control s-form-v4__input g-padding-l-0--xs" rows="5" placeholder="* What are your requirement?" id="requirement"></textarea>
                                </div>
                                <div class="g-text-center--xs">
                                    <button type="submit" class="text-uppercase s-btn s-btn--md s-btn--primary-bg g-radius--50 g-padding-x-70--xs g-margin-b-20--xs" id="btnSoftwareRequirement">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Form -->

                    <!-- Contacts -->
                    <div class="col-md-4 g-bg-color--primary-ltr js__form-eqaul-height-v1">
                        <div class="g-overflow--hidden g-padding-x-40--xs g-padding-y-50--xs">
                            <h2 class="g-font-size-24--xs g-color--white g-margin-b-50--xs">Contact Us</h2>
                            <ul class="list-unstyled g-margin-b-70--xs">
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-location-pin"></i>
                                    </div>
                                    <div class="g-media__body">
                                        Veroda, Cuncolim Salcete, <br> Goa INDIA
                                    </div>
                                </li>
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-headphone-alt"></i>
                                    </div>
                                    <div class="g-media__body">
                                        <a href="tel:+918408881080" style="color:white">+91 8408881080</a><br>
                                        <a href="tel:+917719855705" style="color:white">+91 7719855705</a><br>
                                        <a href="tel:+917387352248" style="color:white">+91 7387352248</a><br>
                                    </div>
                                </li>
                                <li class="clearfix g-color--white g-margin-b-40--xs">
                                    <div class="g-media g-width-40--xs g-margin-t-5--xs">
                                        <i class="g-font-size-20--xs g-color--white-opacity-light ti-email"></i>
                                    </div>
                                    <div class="g-media__body">
                                        business@casfertechnologies.com
                                    </div>
                                </li>
                            </ul>
                            <ul class="list-inline g-ul-li-lr-15--xs">
                                <li><a href="https://www.facebook.com/casfertech"><i class="g-font-size-20--xs g-color--white-opacity ti-facebook"></i></a></li>
                                <li><a href="https://twitter.com/casfertech"><i class="g-font-size-20--xs g-color--white-opacity ti-twitter"></i></a></li>
                                <li><a href="skype:casfertechnologies"><i class="g-font-size-20--xs g-color--white-opacity ti-skype"></i></a></li>
                            </ul>
                            <i class="g-font-size-150--xs g-color--white-opacity-lightest ti-comments" style="position: absolute; bottom: -1.25rem; right: -1.25rem;"></i>
                        </div>
                    </div>
                    <!-- End Contacts -->
                </div>
            </div>
        </div>
        <!-- End Form -->
        <!--========== END PAGE CONTENT ==========-->

        <!--========== FOOTER ==========-->
        <?php include_once("footer.php") ?>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="s-back-to-top js__back-to-top"></a>

        <!--========== JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) ==========-->
        <!-- Vendor -->
        <script type="text/javascript" src="vendor/jquery.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.migrate.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.back-to-top.min.js"></script>
        <script type="text/javascript" src="vendor/scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="vendor/swiper/swiper.jquery.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/jquery.masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/masonry/imagesloaded.pkgd.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.equal-height.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.parallax.min.js"></script>
        <script type="text/javascript" src="vendor/jquery.wow.min.js"></script>

        <!-- General Components and Settings -->
        <script type="text/javascript" src="js/global.min.js"></script>
        <script type="text/javascript" src="js/components/header-sticky.min.js"></script>
        <script type="text/javascript" src="js/components/scrollbar.min.js"></script>
        <script type="text/javascript" src="js/components/swiper.min.js"></script>
        <script type="text/javascript" src="js/components/masonry.min.js"></script>
        <script type="text/javascript" src="js/components/equal-height.min.js"></script>
        <script type="text/javascript" src="js/components/parallax.min.js"></script>
        <script type="text/javascript" src="js/components/wow.min.js"></script>
        <script type="text/javascript" src="vendor/sweetalert/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="js/mails-sendgrid.js"></script>
        <!--========== END JAVASCRIPTS ==========-->

    </body>
    <!-- End Body -->
</html>
