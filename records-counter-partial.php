    <div class="js__parallax-window" style="background: url(img/1920x1080/06.jpg) 50% 0 no-repeat fixed;">
        <div class="container g-padding-y-80--xs g-padding-y-125--sm">
            <div class="row">
                <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--lg">
                    <div class="g-text-center--xs">
                        <div class="g-margin-b-10--xs">
                            <figure class="g-display-inline-block--xs g-font-size-70--xs g-color--white js__counter">11</figure>
                            <span class="g-font-size-40--xs g-color--white">+</span>
                        </div>
                        <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                        <h4 class="g-font-size-18--xs g-color--white">Clients</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--lg">
                    <div class="g-text-center--xs">
                        <div class="g-margin-b-10--xs">
                            <figure class="g-display-inline-block--xs g-font-size-70--xs g-color--white js__counter">15</figure>
                            <span class="g-font-size-40--xs g-color--white">+</span>
                        </div>
                        <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                        <h4 class="g-font-size-18--xs g-color--white">Projects</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6 g-full-width--xs g-margin-b-70--xs g-margin-b-0--sm">
                    <div class="g-text-center--xs">
                        <figure class="g-display-block--xs g-font-size-70--xs g-color--white g-margin-b-10--xs js__counter">34000</figure>
                        <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                        <h4 class="g-font-size-18--xs g-color--white">Miles covered</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-6 g-full-width--xs">
                    <div class="g-text-center--xs">
                        <div class="g-margin-b-10--xs">
                            <figure class="g-display-inline-block--xs g-font-size-70--xs g-color--white js__counter">3</figure>
                            <span class="g-font-size-40--xs g-color--white"></span>
                        </div>
                        <div class="center-block g-hor-divider__solid--white g-width-40--xs g-margin-b-25--xs"></div>
                        <h4 class="g-font-size-18--xs g-color--white">Continents</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>