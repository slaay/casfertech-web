
<?php

//
// SendGrid PHP Library Example
//
// This example shows how to send email through SendGrid
// using the SendGrid PHP Library.  For more information
// on the SendGrid PHP Library, visit:
//
//     https://github.com/sendgrid/sendgrid-php
//

require("sendgrid-php.php");

try {
/* USER CREDENTIALS
/  Fill in the variables below with your SendGrid
/  username and password.
====================================================*/
$sg_username = "presley.diaslee@gmail.com";
$sg_password = "presley123";


/* CREATE THE SENDGRID MAIL OBJECT
====================================================*/
$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();


/*
    Mail details uploaded-images
*/

$valueName = $_POST['customerName'];
$valueEmail = $_POST['customerEmail'];
$valuePhone = $_POST['customerPhone'];
$valueParts = $_POST['customerParts'];
$valueAddress = $_POST['customerAdress'];
$valueReference = $_POST['customerReference'];

$sourcePath = $_FILES['image_file']['tmp_name'];
/* ADD THE ATTACHMENT
/  For the purposes of this demo, the file being
/  attached resides in the same folder as this
/  example.php file
====================================================*/


$target_dir = "uploaded-images/";
//$target_file = $target_dir.basename($_FILES["image_file"]["name"]);
$target_file = $target_dir."upload.png";
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["image_file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 1;
    unlink($target_file);
}
// Check file size
if ($_FILES["image_file"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["image_file"]["tmp_name"], $target_file)) {
       $mail->addAttachment(dirname( __FILE__ )."/".$target_file);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}





/* SMTP API
====================================================*/
// ADD THE RECIPIENTS
$emails = array (
    "business@casfertechnologies.com"
);
$mail->setSmtpapiTos($emails);

// ADD THE CATEGORIES
$categories = array (
    "New parts equiry"
);
foreach($categories as $category) {
    $mail->addCategory($category);
}


/* SEND MAIL
/  Replace the the address(es) in the setTo/setTos
/  function with the address(es) you're sending to.
====================================================*/
    $mail->
    setFrom( "business@casfertechnologies.com" )->
    setSubject( "Customer Enquiry - ".$valueName)->
setText( "Hello,\n\n There is a new enquiry. The details are listed below" )->
    setHtml( "<table style=\"border: 1px solid black; background-color: #F2F4F4; font-family: verdana, tahoma, sans-serif; color: #17202A; width: 100%;\"> 
        <tr> 
            <th>Sr. No.</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email ID</th> 
            <th>Parts required</th>
            <th>Address</th>
            <th>Reference</th>
        </tr> 
        <tr>
            <td>1</td>
            <td>".$valueName."</td>
            <td>".$valuePhone."</td>
            <td>".$valueEmail."</td>
            <td>".$valueParts."</td>
            <td>".$valueAddress."</td>
            <td>".$valueReference."</td>
        </tr>
        </table>" );

    $sendgrid->send( $mail );

   
} catch ( Exception $e ) {
    echo "Unable to send mail: ", $e->getMessage();
}
?>
