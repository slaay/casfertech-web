<?php

//
// SendGrid PHP Library Example
//
// This example shows how to send email through SendGrid
// using the SendGrid PHP Library.  For more information
// on the SendGrid PHP Library, visit:
//
//     https://github.com/sendgrid/sendgrid-php
//

require("sendgrid-php.php");


/* USER CREDENTIALS
/  Fill in the variables below with your SendGrid
/  username and password.
====================================================*/
$sg_username = "presley";
$sg_password = "presley123";


/* CREATE THE SENDGRID MAIL OBJECT
====================================================*/
$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();


/*
    Mail details 
*/

    $customerName = $_POST['customerName'];
    $customerPhone =  $_POST['customerPhone'];
    $customerEmail =  $_POST['customerEmail'];
    $customerParts =  $_POST['customerParts'];
    // $customerPhone =  $_POST['customerPhone'];


/* ADD THE ATTACHMENT
/  For the purposes of this demo, the file being
/  attached resides in the same folder as this
/  example.php file
====================================================*/
$mail->
addAttachment( dirname( __FILE__ )."/sendgrid_logo.jpg" );



/* SMTP API
====================================================*/
// ADD THE RECIPIENTS
$emails = array (
    "sparekaboss@gmail.com"
);
$mail->setSmtpapiTos($emails);

// ADD THE CATEGORIES
$categories = array (
    "New parts equiry"
);
foreach($categories as $category) {
    $mail->addCategory($category);
}


/* SEND MAIL
/  Replace the the address(es) in the setTo/setTos
/  function with the address(es) you're sending to.
====================================================*/
try {
    $mail->
    setFrom( "contact@sparekaboss.com" )->
    setSubject( "Part images" )->
    setText( "Hello,\n\nThis is a test message from SendGrid.    We have sent this to you because you requested a test message be sent from your account.\n\nThis is a link to google.com: http://www.google.com\nThis is a link to apple.com: http://www.apple.com\nThis is a link to sendgrid.com: http://www.sendgrid.com\n\nThank you for reading this test message.\n\nLove,\nYour friends at SendGrid" )->
    setHtml( "<table style=\"border: solid 1px #000; background-color: #666; font-family: verdana, tahoma, sans-serif; color: #fff;\"> <tr> <td> <h2>Hello,</h2> <p>This is a test message from SendGrid.    We have sent this to you because you requested a test message be sent from your account.</p> <a href=\"http://www.google.com\" target=\"_blank\">This is a link to google.com</a> <p> <a href=\"http://www.apple.com\" target=\"_blank\">This is a link to apple.com</a> <p> <a href=\"http://www.sendgrid.com\" target=\"_blank\">This is a link to sendgrid.com</a> </p> <p>Thank you for reading this test message.</p> Love,<br/> Your friends at SendGrid</p> <p> <img src=\"http://cdn1.sendgrid.com/images/sendgrid-logo.png\" alt=\"SendGrid!\" /> </td> </tr> </table>" );
    $sendgrid->send( $mail );

    echo "Sent mail successfully.";
} catch ( Exception $e ) {
    echo "Unable to send mail: ", $e->getMessage();
}


?>
