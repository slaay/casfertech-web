<?php

//
// SendGrid PHP Library Example
//
// This example shows how to send email through SendGrid
// using the SendGrid PHP Library.  For more information
// on the SendGrid PHP Library, visit:
//
//     https://github.com/sendgrid/sendgrid-php
//

require("sendgrid-php.php");


/* USER CREDENTIALS
/  Fill in the variables below with your SendGrid
/  username and password.
====================================================*/
$sg_username = "presley.diaslee@gmail.com";
$sg_password = "presley123";


/* CREATE THE SENDGRID MAIL OBJECT
====================================================*/
$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();


/*
    Mail details 
*/

$valueName = $_POST['name'];
$valueEmail = $_POST['email'];
$valueMessage = $_POST['message'];
$valuePhone = $_POST['phone'];

/* ADD THE ATTACHMENT
/  For the purposes of this demo, the file being
/  attached resides in the same folder as this
/  example.php file
====================================================*/
// $mail->
// addAttachment( $customerFile);


/* SMTP API
====================================================*/
// ADD THE RECIPIENTS
$emails = array (
    "business@casfertechnologies.com"
);
$mail->setSmtpapiTos($emails);

// ADD THE CATEGORIES
$categories = array (
    "New contact mail"
);
foreach($categories as $category) {
    $mail->addCategory($category);
}


/* SEND MAIL
/  Replace the the address(es) in the setTo/setTos
/  function with the address(es) you're sending to.
====================================================*/
try {
    $mail->
    setFrom( "business@casfertechnologies.com" )->
    setSubject($valueName." - has sent you a message")->
setText( "Hello,\n\n There is a contact message. The details are listed below" )->
    setHtml( "<table style=\"border: solid 1px #000; background-color: #F2F4F4; font-family: verdana, tahoma, sans-serif; color: #17202A;\"> 
        <tr> 
            <th>Name</th>
            <th>Email ID</th>
            <th>Phone</th> 
            <th>Message</th>
        </tr> 
        <tr>
            <td>".$valueName."</td>
            <td>".$valueEmail."</td>
            <td>".$valuePhone."</td>
            <td>".$valueMessage."</td>
        </tr>
        </table>" );


    $sendgrid->send( $mail );

    
} catch ( Exception $e ) {
    echo "Unable to send mail: ", $e->getMessage();
}
?>
