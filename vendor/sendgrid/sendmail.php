<?php

//
// SendGrid PHP Library Example
//
// This example shows how to send email through SendGrid
// using the SendGrid PHP Library.  For more information
// on the SendGrid PHP Library, visit:
//
//     https://github.com/sendgrid/sendgrid-php
//

require("sendgrid-php.php");


/* USER CREDENTIALS
/  Fill in the variables below with your SendGrid
/  username and password.
====================================================*/
$sg_username = "presley";
$sg_password = "presley123";


/* CREATE THE SENDGRID MAIL OBJECT
====================================================*/
$sendgrid = new SendGrid( $sg_username, $sg_password );
$mail = new SendGrid\Email();


/*
    Mail details 
*/

$valueName = $_POST['name'];
$valueEmail = $_POST['email'];
$valueMessage = $_POST['message'];

/* ADD THE ATTACHMENT
/  For the purposes of this demo, the file being
/  attached resides in the same folder as this
/  example.php file
====================================================*/
// $mail->
// addAttachment( $customerFile);


/* SMTP API
====================================================*/
// ADD THE RECIPIENTS
$emails = array (
    "presley.diaslee@gmail.com",
    "edviges29@gmail.com"
);
$mail->setSmtpapiTos($emails);

// ADD THE CATEGORIES
$categories = array (
    "New parts equiry"
);
foreach($categories as $category) {
    $mail->addCategory($category);
}


/* SEND MAIL
/  Replace the the address(es) in the setTo/setTos
/  function with the address(es) you're sending to.
====================================================*/
try {
    $mail->
    setFrom( "business@casfertechnologies.com" )->
    setSubject( "Customer contact")->
    setText("Customer Name :".$valueName."  Customer Email :".$valueEmail. " " . "Message : ".$valueMessage);
    $sendgrid->send( $mail );

    
} catch ( Exception $e ) {
    echo "Unable to send mail: ", $e->getMessage();
}
?>
